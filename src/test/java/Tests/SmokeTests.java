package Tests;

import BaseTests.BaseTests;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class SmokeTests extends BaseTests {

    @Test
    public void AAC_01_FLCA_Mortgage (){
        System.out.println("[INFO]: 'FLCA Mortgage' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.FlcaLoanType);
        pp.selectValue(pp.ProductSelectorField, pp.FlcaMortgageProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        //getWebDriver().switchTo().defaultContent();
        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'FLCA Mortgage' test end *****");
    }

    @Test
    public void AAC_02_FLCA_RealEstate (){
        System.out.println("[INFO]: 'FLCA Real Estate' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.FlcaLoanType);
        pp.selectValue(pp.ProductSelectorField, pp.FlcaRealEstateProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'FLCA Real Estate' test end *****");
    }

    @Test
    public void AAC_03_Lease_Equipment (){
        System.out.println("[INFO]: 'Lease Equipment' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.LeaseType);
        pp.selectValue(pp.ProductSelectorField, pp.LeaseEquipmentProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'Lease Equipment' test end *****");
    }

    @Test
    public void AAC_04_Lease_Facility (){
        System.out.println("[INFO]: 'Lease Facility' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.LeaseType);
        pp.selectValue(pp.ProductSelectorField, pp.LeaseFacilityProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'Lease Facility' test end *****");
    }

    @Test
    public void AAC_05_Lease_Fleet (){
        System.out.println("[INFO]: 'Lease Fleet' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.LeaseType);
        pp.selectValue(pp.ProductSelectorField, pp.LeaseFleetProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'Lease Fleet' test end *****");
    }

    @Test
    public void AAC_06_PCA_Intermediate (){
        System.out.println("[INFO]: 'PCA Intermediate' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.PcaLoanType);
        pp.selectValue(pp.ProductSelectorField, pp.PcaIntermediateProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'PCA Intermediate' test end *****");
    }

    @Test
    public void AAC_07_PCA_NonRevolving (){
        System.out.println("[INFO]: 'PCA Non-Revolving' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.PcaLoanType);
        pp.selectValue(pp.ProductSelectorField, pp.PcaNonRevolvingProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'PCA Non-Revolving' test end *****");
    }

    @Test
    public void AAC_08_PCA_Revolving (){
        System.out.println("[INFO]: 'PCA Revolving' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.PcaLoanType);
        pp.selectValue(pp.ProductSelectorField, pp.PcaRevolvingProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'PCA Revolving' test end *****");
    }

    @Test
    public void AAC_09_PCA_StandAlone (){
        System.out.println("[INFO]: 'PCA Stand Alone' test start *****");
        System.out.println("[INFO]: Navigate to Product Package.");
        open(pp.ProductPackageUrl);

        pp.ProductPackageIframe.waitUntil(exist, 60000);
        getWebDriver().switchTo().frame(pp.ProductPackageIframe);

        System.out.println("[INFO]: Click on 'Magic' button.");
        $(pp.MagicButton).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Click on 'New Facility'.");
        $(pp.NewFacility).waitUntil(visible, 60000);
        $(pp.NewFacility).waitUntil(enabled, 60000).click();

        System.out.println("[INFO]: Fill all required fields.");
        pp.selectValue(pp.LineSelectorField, pp.RetailLine);
        pp.selectValue(pp.TypeSelectorField, pp.PcaLoanType);
        pp.selectValue(pp.ProductSelectorField, pp.PcaStandAloneProduct);
        pp.selectValue(pp.BorrowerTypeSelectorField, pp.BorrowerType);

        System.out.println("[INFO]: Click on 'Create New Loan' button.");
        pp.CreateNewLoanButton.waitUntil(enabled, 60000).click();

        getWebDriver().switchTo().parentFrame();

        System.out.println("[INFO]: Check data in new Loan.");
        loan.LoanPipeline.waitUntil(visible, 30000);
        loan.LoanHeaderAccount.waitUntil(exist, 60000);
        loan.LoanHeaderAccount.shouldHave(text("PETER H WONG"));
        System.out.println("[INFO]: 'PCA Stand Alone' test end *****");
    }
}
