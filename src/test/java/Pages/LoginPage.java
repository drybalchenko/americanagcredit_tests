package Pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {

    //---Salesforce
    @FindBy(xpath = "//input[@id='username']")
    public SelenideElement SlLoginField;

    @FindBy(xpath = "//input[@id='password']")
    public SelenideElement SlPasswordField;

    @FindBy(xpath = "//input[@id='Login']")
    public SelenideElement SlLoginButton;

    @FindBy(xpath = "//a[@class='switch-to-lightning']")
    public SelenideElement SwitchToLightningLink;

    @FindBy(css = ".slds-icon-waffle")
    public SelenideElement AppLauncherIcon;

    @FindBy(xpath = "//input[@id='thePage:inputForm:continue']")
    public SelenideElement ContinueButton;


    public LoginPage loginToSalesforce() {
        System.out.println("[INFO]: Login to Salesforce with Login and Password.");
        open("https://agloan--sldev.my.salesforce.com");
        $(SlLoginField).waitUntil(enabled, 30000).setValue("dmitry.bondarenko@aac.sldev");
        $(SlPasswordField).waitUntil(enabled, 30000).setValue("Silverline2019");
        $(SlLoginButton).waitUntil(enabled, 30000).click();

        try{ if(SwitchToLightningLink.exists()){
            $(SwitchToLightningLink).waitUntil(enabled, 30000).click();
        }} catch (Exception e){}

        $(AppLauncherIcon).waitUntil(enabled, 30000).click();
        return this;
    }

    public LoginPage loginByUrl() throws InterruptedException {
        open("https://agloan--sldev.lightning.force.com/?un=dmitry.bondarenko@aac.sldev&pw=Silverline2019");

        Thread.sleep(3000);
        try {
            if (ContinueButton.exists()) {
                $(ContinueButton).shouldBe(enabled).click(); }
        }
        catch(Exception e){
        }
        return this;
    }
}
