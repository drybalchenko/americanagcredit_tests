package Pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class LoanPage {

    @FindBy(xpath = ".//div[@class='runtime_sales_pathassistantPathAssistant']")
    public SelenideElement LoanPipeline;

    @FindBy(xpath = ".//ul[@role='list']/li[2]//a")
    public SelenideElement LoanHeaderAccount;

    @FindBy(xpath = ".//div[4]/div//header[@role='banner']/div[2]/ul[@role='list']/li[4]//div[@class='slds-form-element__static slds-truncate']//span")
    public SelenideElement LoanHeaderProductLine;

    @FindBy(xpath = "//div[@id='brandBand_1']/div/div[1]/div[4]//div/div[1]/div[1]/header[@role='banner']/div[2]/ul[@role='list']/li[5]//span[.='PCA Loan']")
    public SelenideElement LoanHeaderProductType;



}
