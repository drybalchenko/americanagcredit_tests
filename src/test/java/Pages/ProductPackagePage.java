package Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class ProductPackagePage {

    public String ProductPackageUrl1 = "https://agloan--sldev.lightning.force.com/lightning/r/LLC_BI__Product_Package__c/a3b56000000IGX9AAO/view";
    public String ProductPackageUrl2 = "https://agloan--sldev.lightning.force.com/lightning/r/LLC_BI__Product_Package__c/a3b56000000IGZ0AAO/view";
    public String ProductPackageUrl = "https://agloan--sldev.lightning.force.com/lightning/r/LLC_BI__Product_Package__c/a3b56000000IGX9AAO/view"; //"https://agloan--sldev.lightning.force.com/lightning/r/LLC_BI__Product_Package__c/a3b56000000IGxhAAG/view";

    @FindBy(xpath =".//div[@class='force slds']//button[@id='tools-actions']")
    public SelenideElement MagicButton;

    @FindBy(xpath = ".//ul[@class='dropdown-menu dropdown-menu-right']/li[7]/a")
    public SelenideElement NewFacility;

    //---New Loan Form
    @FindBy(xpath = ".//select[@id='lineSelector']")
    public SelenideElement LineSelectorField;

    @FindBy(xpath = ".//select[@id='lineSelector']/option[@label='Commercial']")
    public SelenideElement CommercialLine;

    @FindBy(xpath = ".//select[@id='lineSelector']/option[@label='Retail']")
    public SelenideElement RetailLine;

    //----------
    @FindBy(xpath = ".//select[@id='typeSelector']")
    public SelenideElement TypeSelectorField;

    @FindBy(xpath = ".//select[@id='typeSelector']/option[@label='FLCA Loan']")
    public SelenideElement FlcaLoanType;

    @FindBy(xpath = ".//select[@id='typeSelector']/option[@label='Lease']")
    public SelenideElement LeaseType;

    @FindBy(xpath = ".//select[@id='typeSelector']/option[@label='PCA Loan']")
    public SelenideElement PcaLoanType;

    //----------
    @FindBy(xpath = ".//select[@id='productSelector']")
    public SelenideElement ProductSelectorField;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Mortgage Loan']")
    public SelenideElement FlcaMortgageProduct;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Real Estate Line of Credit (RELOC)']")
    public SelenideElement FlcaRealEstateProduct;
    //---
    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Equipment Lease']")
    public SelenideElement LeaseEquipmentProduct;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Facility Lease']")
    public SelenideElement LeaseFacilityProduct;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Fleet Lease Program']")
    public SelenideElement LeaseFleetProduct;
    //---
    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Intermediate Term Loan (ITL)']")
    public SelenideElement PcaIntermediateProduct;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Non Revolving Line of Credit']")
    public SelenideElement PcaNonRevolvingProduct;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Revolving Line of Credit (RLOC)']")
    public SelenideElement PcaRevolvingProduct;

    @FindBy(xpath = ".//select[@id='productSelector']/option[@label='Stand Alone Letter of Credit']")
    public SelenideElement PcaStandAloneProduct;

    //----------
    @FindBy(xpath = ".//select[@id='borrowerTypeSelector']")
    public SelenideElement BorrowerTypeSelectorField;

    @FindBy(xpath = ".//select[@id='borrowerTypeSelector']/option[@label='Borrower']")
    public SelenideElement BorrowerType;

    @FindBy(xpath = ".//select[@id='borrowerTypeSelector']/option[@label='Co-Borrower']")
    public SelenideElement CoBorrowerType;

    @FindBy(xpath = ".//select[@id='borrowerTypeSelector']/option[@label='Recourse Guarantor']")
    public SelenideElement RecourseGuarantorBorrower;

    @FindBy(xpath = ".//select[@id='borrowerTypeSelector']/option[@label='Non-Recourse Guarantor']")
    public SelenideElement NonRecourseGuarantorBorrower;

    //---Buttons
    @FindBy(xpath = ".//button[contains (text(), 'Create New Loan')]")
    public SelenideElement CreateNewLoanButton;

    //---iFrame
    @FindBy(xpath = ".//iframe[@title='accessibility title']")
    public SelenideElement ProductPackageIframe;


    //---Methods

    public void selectValue (SelenideElement field, SelenideElement value){
        field.waitUntil(enabled, 60000).click();
        value.waitUntil(enabled, 60000).click();
    }
}
