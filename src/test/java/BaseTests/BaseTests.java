package BaseTests;

import Pages.LoanPage;
import Pages.LoginPage;
import Helpers.BrowsersHelper;
import Pages.ProductPackagePage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import com.codeborne.selenide.WebDriverRunner;

import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

public class BaseTests {

    public static LoginPage lp;
    public static LoanPage loan;
    public static ProductPackagePage pp;


    @BeforeAll
    public static void setup() throws InterruptedException {
        setWebDriver(BrowsersHelper.getChromeLocalWebDriver());
        //setWebDriver(BrowsersHelper.getChromeRemoteWebDriver());
        initializePages();
        lp.loginToSalesforce();
        //lp.loginByUrl();
    }

    public static void initializePages() {
        lp = page (LoginPage.class);
        loan = page (LoanPage.class);
        pp = page (ProductPackagePage.class);
    }

    @AfterAll
    public static void end(){
        WebDriverRunner.getWebDriver().quit();
    }
}
